/*
 * Copyright (C) 2020 Mikhail Zolotukhin <zomial@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <exception>
#include <atomic>

namespace nerv {

template<typename T>
class tagged_ptr
{
public:
    tagged_ptr() noexcept : ptr(nullptr), tag(0) {}
    explicit tagged_ptr(T* p) noexcept : ptr(p), tag(0) {}
    tagged_ptr(T* p, unsigned int n) noexcept : ptr(p), tag(n) {}

    T * operator->() const { return ptr; }
    T operator*() const { return *ptr; }
    bool operator==(const tagged_ptr &rhs) const
    {
        return ptr == rhs.ptr &&
            tag == rhs.tag;
    }
    bool operator!=(const tagged_ptr &rhs) const
    {
        return !(rhs == *this);
    }
    T* ptr;
    unsigned int tag;
};

template <typename T>
struct node
{
    node()
        : value{}
        , next{}
    {}

    node(const node<T>& rhs)
        : value(rhs.value), next(rhs.next.load())
    {}

    node(T value, const tagged_ptr<node<T>> &next)
        : value{value}
        , next{next}
    {}

    T value;
    std::atomic<tagged_ptr<node<T>>> next;
};


template <typename T>
class k_lock_free_queue
{
public:
    k_lock_free_queue()
        : head{tagged_ptr<node<T>>{new node<T>{}}}
        , tail{head.load()}
    {}

    void enque(const T& new_element)
    {
        tagged_ptr<node<T>> new_node (new node<T>(new_element, tagged_ptr<node<T>>()));
        while (true) {
            auto tail_copy = tail.load();
            auto empty_ptr = tagged_ptr<node<T>>();

            if (tail_copy->next.compare_exchange_strong(empty_ptr, new_node)) {
                tail.compare_exchange_strong(tail_copy, new_node);
                return;
            } else {
                tail.compare_exchange_strong(tail_copy, tail_copy->next.load());
            }
        }
    }

    T deque()
    {
        while (true) {
            auto head_copy = head.load();
            auto tail_copy = tail.load();
            auto head_next_copy = head_copy->next.load();
            if (head_copy == tail_copy) {
                if (head_next_copy == tagged_ptr<node<T>>()) {
                    // Empty queue
                    throw std::exception();
                }
            } else {
                auto result = (*head_next_copy).value;
                if (head.compare_exchange_strong(head_copy, head_next_copy)) {
                    delete head_next_copy.ptr;
                    return result;
                }
            }
        }
    }

private:
    std::atomic<tagged_ptr<node<T>>> head;
    std::atomic<tagged_ptr<node<T>>> tail;
};

}

