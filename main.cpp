/*
 * Copyright (C) 2020 Mikhail Zolotukhin <zomial@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <chrono>
#include <thread>
#include <vector>

#include "k_lock_free_queue.h"

template<typename T>
auto create_thread_to_add_n_numbers(nerv::k_lock_free_queue<T>& queue, int n)
{
    return new std::thread {[&](){
        for (int i = 0; i < n; i++) {
//            queue.enque(42);
        }
    }};
}

template<typename T>
auto create_thread_to_remove_n_numbers(nerv::k_lock_free_queue<T>& queue, int n)
{
    return new std::thread {[&](){
        for (int i = 0; i < n; i++) {
//            queue.deque();
        }
    }};
}

void measure_for_n_threads(int n)
{
    static const int number_of_additions {10000};
    int portion_for_thread = number_of_additions / n;
    nerv::k_lock_free_queue<int> queue {};

    std::vector<std::thread*> threads_list {};
    threads_list.reserve(n);

    auto t1 = std::chrono::high_resolution_clock::now();

    for (int i = 0; i < n; i++) {
        threads_list.emplace_back(create_thread_to_add_n_numbers(queue, portion_for_thread));
    }

    auto t2 = std::chrono::high_resolution_clock::now();

    for (auto* thread_ptr : threads_list) {
        thread_ptr->join();
        delete thread_ptr;
    }

    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

    std::cout << "Enque for " << n << " threads: " << duration << " microseconds" << std::endl;


    threads_list.clear();

    t1 = std::chrono::high_resolution_clock::now();

    for (int i = 0; i < n; i++) {
        threads_list.emplace_back(create_thread_to_remove_n_numbers(queue, portion_for_thread));
    }

    t2 = std::chrono::high_resolution_clock::now();

    for (auto* thread_ptr : threads_list) {
        thread_ptr->join();
        delete thread_ptr;
    }

    duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

    std::cout << "Deque for " << n << " threads: " << duration << " microseconds" << std::endl;
}

void test()
{
    measure_for_n_threads(1);
    measure_for_n_threads(2);
    measure_for_n_threads(4);
    measure_for_n_threads(8);
    measure_for_n_threads(16);
    measure_for_n_threads(32);
}

int main()
{
    test();
    return 0;
}
